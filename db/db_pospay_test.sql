/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100413
 Source Host           : localhost:3306
 Source Schema         : db_pospay_test

 Target Server Type    : MySQL
 Target Server Version : 100413
 File Encoding         : 65001

 Date: 29/10/2021 21:52:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (12, 'Kas Perusahaan', '2021-08-24 17:56:20', '2021-10-16 02:57:46');
INSERT INTO `categories` VALUES (13, 'Konsumsi', '2021-08-24 17:56:31', '2021-08-27 20:48:38');
INSERT INTO `categories` VALUES (14, 'Rumah Tangga', '2021-08-24 17:56:38', '2021-08-27 20:48:55');
INSERT INTO `categories` VALUES (15, 'Transportasi', '2021-08-24 18:09:28', '2021-08-27 20:49:15');
INSERT INTO `categories` VALUES (16, 'Sewa', '2021-08-24 22:33:11', '2021-08-27 20:49:28');
INSERT INTO `categories` VALUES (17, 'Klien', '2021-08-27 20:50:00', '2021-08-27 20:50:00');
INSERT INTO `categories` VALUES (18, 'Lainnya', '2021-08-27 20:50:03', '2021-08-27 20:50:03');
INSERT INTO `categories` VALUES (19, 'BPJS', '2021-08-27 20:50:58', '2021-08-27 20:50:58');

-- ----------------------------
-- Table structure for pln
-- ----------------------------
DROP TABLE IF EXISTS `pln`;
CREATE TABLE `pln`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `jenis` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_pelanggan` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `amount` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`, `id_user`) USING BTREE,
  INDEX `fk_id_user`(`id_user`) USING BTREE,
  CONSTRAINT `fk_id_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pln
-- ----------------------------
INSERT INTO `pln` VALUES (47, 1, 'TOKEN PLN', '900', 900, '2021-10-29 16:45:16', '2021-10-29 16:45:16');
INSERT INTO `pln` VALUES (48, 1, 'TOKEN PLN', '900', 900, '2021-10-29 16:47:07', '2021-10-29 16:47:07');

-- ----------------------------
-- Table structure for transactions
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` enum('income','expense') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `amount` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `key_category_id`(`category_id`) USING BTREE,
  CONSTRAINT `key_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transactions
-- ----------------------------
INSERT INTO `transactions` VALUES (4, 13, 'Rapat dengan klien', 'expense', 20000, '2021-08-24 19:53:10', '2021-08-27 20:50:44');
INSERT INTO `transactions` VALUES (6, 15, 'Biaya Transportasi pengiriman barang', 'expense', 250000, '2021-08-24 19:53:10', '2021-08-27 20:50:33');
INSERT INTO `transactions` VALUES (7, 12, 'Iuran kas bulan Juli', 'income', 1000000, '2021-08-24 21:02:48', '2021-08-27 20:52:09');
INSERT INTO `transactions` VALUES (8, 13, 'Donasi anonim', 'income', 53000, '2021-08-24 21:13:21', '2021-08-24 21:13:21');
INSERT INTO `transactions` VALUES (17, 19, 'Bayar BPJS bulan maret', 'expense', 250000, '2021-08-24 22:30:28', '2021-08-27 20:51:24');
INSERT INTO `transactions` VALUES (18, 12, 'Beli Buku Baru', 'expense', 10000, '2021-08-24 23:06:25', '2021-08-27 20:51:41');
INSERT INTO `transactions` VALUES (19, 17, 'Pesangon project berhasil', 'income', 281000, '2021-08-27 20:52:33', '2021-08-27 20:52:41');
INSERT INTO `transactions` VALUES (20, 18, 'Bonus cashback pembelian barang ATK', 'income', 9000, '2021-08-27 20:53:28', '2021-08-27 20:53:28');
INSERT INTO `transactions` VALUES (21, 14, 'Ganti rugi biaya meal setiap karyawan', 'expense', 830000, '2021-08-27 20:54:05', '2021-08-27 20:54:05');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `remember_token` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 0,
  `role` enum('employee','admin') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `referal` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `saldo` int(11) NOT NULL DEFAULT 0,
  `poin` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`, `phonenumber`, `username`, `email`) USING BTREE,
  UNIQUE INDEX `u_username`(`username`) USING BTREE,
  UNIQUE INDEX `u_phone`(`phonenumber`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '0812342345233', 'robi', 'robi@gmail.com', 'robi1', 'robi zulva', NULL, '0000-00-00 00:00:00', '2021-10-16 06:09:49', 1, 'admin', '', 48100, 372);
INSERT INTO `users` VALUES (2, '0812345235235', 'irfanrona', 'irfan.rona95@student.upi.edu', 'irfanrona', 'Irfan Rona', NULL, '2021-08-27 19:28:27', '2021-08-27 20:05:43', 1, 'employee', NULL, 1250000, 120);
INSERT INTO `users` VALUES (3, '0812345674342', 'jamet', 'jamet@mail.com', 'jamet', 'jamet raya', NULL, '2021-08-27 20:05:17', '2021-08-27 20:42:30', 0, 'admin', NULL, 48000, 50);
INSERT INTO `users` VALUES (4, '0812345678341', 'ronapospay', 'irfan.rona95@student.upi.edu', 'modalketawa', 'Rona Pospay', NULL, '2021-10-16 02:22:07', '2021-10-16 02:22:07', 0, 'employee', NULL, 29045000, 7);

SET FOREIGN_KEY_CHECKS = 1;
