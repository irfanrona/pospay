<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
	}

	public function index()
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		$data['title'] = "Finance App";
		$data['headerContent'] = "User";
		$data['active'] = "UserManagement";

		//query
		$data['query'] = $this->UserModel->get_last_ten_entries();

		$data['content'] = $this->load->view('admin/user/main', $data, TRUE);
		$this->load->view('main_layout', $data);
	}

	public function add()
	{
		if (isset($_POST['repassword'])) {
			if ($_POST['repassword'] != $_POST['password']) {
				$this->session->set_flashdata('flash', 'Password tidak sama');
				redirect(base_url('auth/register'), 'refresh');
			}
		}
		$check = $this->UserModel->get_username($_POST['username']);
		if (@$check[0]->username == $_POST['username']) {
			$this->session->set_flashdata('flash', 'username sudah digunakan');

			if ($this->session->has_userdata('role')) {
				redirect(base_url('user'), 'refresh');
			} else {
				redirect(base_url('auth/register'), 'refresh');
			}
		}
		$check = $this->UserModel->get_phonenumber($_POST['phonenumber']);
		if (@$check[0]->phonenumber == $_POST['phonenumber']) {
			$this->session->set_flashdata('flash', 'no. telepon sudah digunakan');

			if ($this->session->has_userdata('role')) {
				redirect(base_url('user'), 'refresh');
			} else {
				redirect(base_url('auth/register'), 'refresh');
			}
		}
		//query
		$data['query'] = $this->UserModel->insert_entry();
		//flashdata
		$this->session->set_flashdata('flash', 'User berhasil dibuat');
		if ($this->session->has_userdata('role')) {
			redirect(base_url('user'), 'refresh');
		} else {
			redirect(base_url('auth'), 'refresh');
		}
	}

	public function edit($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		$data['title'] = "Finance App";
		$data['headerContent'] = "User | Edit";
		$data['active'] = "User";

		//query edit
		$data['query'] = $this->UserModel->get_id($id);

		$data['content'] = $this->load->view('admin/user/edit-user', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function delete($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		//query
		$data['query'] = $this->UserModel->delete_entry($id);
		//flashdata


		redirect(base_url('user'), 'refresh');
	}

	public function update($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		//query
		$data['query'] = $this->UserModel->update_entry($id);
		//flashdata

		redirect(base_url('user'), 'refresh');
	}

	//other function
	public function topup($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		$data['title'] = "Pospay";
		$data['headerContent'] = "Tambah Saldo";
		$data['active'] = "Saldo";

		//query edit
		$data['query'] = $this->UserModel->get_id($id);

		$data['content'] = $this->load->view('admin/user/tambah-saldo', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function penarikan($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		$data['title'] = "Pospay";
		$data['headerContent'] = "Penarikan";
		$data['active'] = "Saldo";

		//query edit
		$data['query'] = $this->UserModel->get_id($id);

		$data['content'] = $this->load->view('admin/user/fitur-belum-tersedia', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function transfer($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		$data['title'] = "Pospay";
		$data['headerContent'] = "Transfer";
		$data['active'] = "Saldo";

		//query edit
		$data['query'] = $this->UserModel->get_id($id);

		$data['content'] = $this->load->view('admin/user/fitur-belum-tersedia', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function aktivasi($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		$data['title'] = "Pospay";
		$data['headerContent'] = "Transfer";
		$data['active'] = "Saldo";

		//query edit
		$data['query'] = $this->UserModel->get_id($id);

		$data['content'] = $this->load->view('admin/user/aktivasi', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function profile($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		$data['title'] = "Pospay";
		$data['headerContent'] = "Profile";
		$data['active'] = "User";

		//query edit
		$data['query'] = $this->UserModel->get_id($id);

		$data['content'] = $this->load->view('admin/user/profile', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function verifikasiAkun($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		$data['title'] = "Pospay";
		$data['headerContent'] = "Verifikasi Akun";
		$data['active'] = "User";

		//query edit
		$data['query'] = $this->UserModel->get_id($id);

		$data['content'] = $this->load->view('admin/user/verifikasi-akun', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function profilSaya($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		$data['title'] = "Pospay";
		$data['headerContent'] = "Profile";
		$data['active'] = "User";

		//query edit
		$data['query'] = $this->UserModel->get_id($id);

		$data['content'] = $this->load->view('admin/user/profil-saya', $data, TRUE);

		$this->load->view('main_layout', $data);
	}
}
