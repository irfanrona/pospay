<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pln extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('PlnModel');
		$this->load->model('UserModel');

		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}
	}

	public function index()
	{
		$data['title'] = "Pospay | PLN";
		$data['headerContent'] = "PLN";
		$data['active'] = "PLN";

		$dataRaw = $this->PlnModel->get_last_ten_entries();

		$data['query'] = $dataRaw;

		$data['content'] = $this->load->view('admin/pln/add-pln', $data, TRUE);
		$data['jscript'] = $this->load->view('admin/pln/js-pln', NULL, TRUE);
		$this->load->view('main_layout', $data);
	}

	public function add()
	{
		$data['title'] = "Pospay | PLN";
		$data['headerContent'] = "PLN";
		$data['active'] = "PLN";

		$user = $this->UserModel->get_name($this->session->userdata('id'));
		if ($user[0]->saldo > $_POST['amount']) {
			//query
			$data['query'] = $this->PlnModel->insert_entry();
			$this->UserModel->set_saldo($this->session->userdata('id'), $user[0]->saldo - $_POST['amount']);
			$data['flashdata'] = "Transaksi Berhasil!";
		} else {
			$data['flashdata'] = "Transaksi Gagal, Saldo anda tidak cukup!";
		}
		//flashdata
		$data['content'] = $this->load->view('admin/pln/status', $data, TRUE);
		$data['jscript'] = $this->load->view('admin/pln/js-pln', NULL, TRUE);
		$this->load->view('main_layout', $data);
	}

	public function edit($id)
	{
		$data['title'] = "Pospay | PLN";

		$data['headerContent'] = "PLN | Edit";
		$data['active'] = "PLN";
		//query edit
		$data['query'] = $this->PlnModel->get_id($id);

		$data['content'] = $this->load->view('admin/pln/edit-pln', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function delete($id)
	{
		//query
		$data['query'] = $this->PlnModel->delete_entry($id);
		//flashdata

		$previous = "javascript:history.go(-1)";
		if (isset($_SERVER['HTTP_REFERER'])) {
			$previous = $_SERVER['HTTP_REFERER'];
		}
		redirect($previous, 'refresh');
	}

	public function update($id)
	{
		//query
		$data['query'] = $this->PlnModel->update_entry($id);
		//flashdata

		redirect(base_url('pln/'), 'refresh');
	}

	public function status($id)
	{
		$data['title'] = "Pospay | PLN";
		$data['headerContent'] = "PLN";
		$data['active'] = "PLN";

		$dataRaw = $this->PlnModel->get_last_ten_entries();

		$data['query'] = $dataRaw;

		$data['content'] = $this->load->view('admin/pln/add-pln', $data, TRUE);
		$data['jscript'] = $this->load->view('admin/pln/js-pln', NULL, TRUE);
		$this->load->view('main_layout', $data);
	}
}
