<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
	}

	public function index()
	{
		if ($this->session->has_userdata('role')) {
			redirect(base_url('admin'), 'refresh');
		} else {
			$data['title'] = "Pospay App";
			$data['headerContent'] = "Login";


			$data['content'] = $this->load->view('auth/login', $data, TRUE);
			$this->load->view('public_layout', $data);
		}
	}

	public function login()
	{
		//get credential
		$user = $this->UserModel->get_username(@$_POST['username']);
		if (!empty($user)) {
			if ($_POST['username'] == $user[0]->username && $_POST['password'] == $user[0]->password) {
				if (!$user[0]->is_active) {
					$this->session->set_flashdata('flash', 'User belum diaktifkan');
				} else {
					//credential match
					$newdata = array(
						'username'  => $user[0]->username,
						'fullname' => $user[0]->fullname,
						'role' => $user[0]->role,
						'id' => $user[0]->id
					);

					$this->session->set_userdata($newdata);
					$this->session->set_flashdata('flash', 'selamat datang ' . $user[0]->fullname);
				}
			} else {
				//credential didnt match
				$this->session->set_flashdata('flash', 'Username atau Password tidak cocok');
			}
		} else {
			// no username
			$this->session->set_flashdata('flash', 'Username tidak ditemukan');
		}
		redirect(base_url('auth'), 'refresh');
		//echo $this->session->flashdata('flash');
	}

	public function register()
	{
		$data['title'] = "Pospay App";
		$data['headerContent'] = "Register";

		$data['content'] = $this->load->view('auth/register', $data, TRUE);
		$this->load->view('public_layout', $data);
	}

	public function password($param)
	{
		$data['title'] = "Pospay App";
		$data['headerContent'] = "Forgot Password";
		if ($param == 'forgot') {
			$data['content'] = $this->load->view('auth/forgot-insert-no', $data, TRUE);
			$this->load->view('public_layout', $data);
		} else if ($param == 'new') {
			//get credential
			$user = $this->UserModel->get_phonenumber(@$_POST['phonenumber']);
			if (!empty($user)) {
				if ($_POST['phonenumber'] == $user[0]->phonenumber) {
					$data['userId'] = $user[0]->id;
					$data['content'] = $this->load->view('auth/forgot-insert-pass', $data, TRUE);
					$this->load->view('public_layout', $data);
				} else {
					//credential didnt match
					$this->session->set_flashdata('flash', 'Terjadi kesalahan');
					$data['content'] = $this->load->view('auth/forgot-insert-no', $data, TRUE);
					$this->load->view('public_layout', $data);
				}
			} else {
				// no username
				$this->session->set_flashdata('flash', 'Nomor telepon tidak diterdaftar');
				$data['content'] = $this->load->view('auth/forgot-insert-no', $data, TRUE);
				$this->load->view('public_layout', $data);
			}
		} else if ($param == 'update') {

			$data['query'] = $this->UserModel->update_pass($_POST['id']);

			$this->session->set_flashdata('flash', 'Ubah Password berhasil');
			redirect(base_url('auth'), 'refresh');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('auth'), 'refresh');
	}
}
