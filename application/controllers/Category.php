<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('CategoryModel');

		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}
	}

	public function index()
	{
		$data['title'] = "Finance App";
		$data['headerContent'] = "Kategori";
		$data['active'] = "Kategori";

		//query
		$data['query'] = $this->CategoryModel->get_last_ten_entries();

		$data['content'] = $this->load->view('admin/category/main', $data, TRUE);
		$this->load->view('main_layout', $data);
	}

	public function add()
	{
		//query
		$data['query'] = $this->CategoryModel->insert_entry();
		//flashdata


		redirect(base_url('category'), 'refresh');
	}

	public function edit($id)
	{
		$data['title'] = "Finance App";

		$data['headerContent'] = "Kategori | Edit";
		$data['active'] = "Kategori";
		//query edit
		$data['query'] = $this->CategoryModel->get_id($id);

		$data['content'] = $this->load->view('admin/category/edit-category', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function delete($id)
	{
		//query
		$data['query'] = $this->CategoryModel->delete_entry($id);
		//flashdata


		redirect(base_url('category'), 'refresh');
	}

	public function update($id)
	{
		//query
		$data['query'] = $this->CategoryModel->update_entry($id);
		//flashdata

		redirect(base_url('category'), 'refresh');
	}
}
