<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('TransactionModel');
		$this->load->model('CategoryModel');

		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}
	}

	public function index()
	{
		$data['title'] = "Finance App";
		$data['headerContent'] = "Laporan";
		$data['active'] = "Laporan";

		$dataRaw = $this->TransactionModel->get_last_ten_entries();

		$i = 0;
		foreach ($dataRaw as $value) {
			$category = $this->CategoryModel->get_name($value->category_id);

			$temp[$i] = new StdClass;
			$temp[$i]->id = $value->id;

			$temp[$i]->category_id = $category[0]->name;
			$temp[$i]->desc = $value->desc;
			$temp[$i]->amount = $value->amount;
			$temp[$i]->type = $value->type;

			$i++;
		}
		$data['query'] = $temp;

		$data['content'] = $this->load->view('admin/transaction/main-transaction', $data, TRUE);
		$data['jscript'] = $this->load->view('admin/transaction/js-transaction', NULL, TRUE);
		$this->load->view('main_layout', $data);
	}

	public function print()
	{
		$data['title'] = "Finance App";
		$data['headerContent'] = "Laporan";
		$data['active'] = "Laporan-print";

		$dataRaw = $this->TransactionModel->get_last_ten_entries();

		$i = 0;
		foreach ($dataRaw as $value) {
			$category = $this->CategoryModel->get_name($value->category_id);

			$temp[$i] = new StdClass;
			$temp[$i]->id = $value->id;

			$temp[$i]->category_id = $category[0]->name;
			$temp[$i]->desc = $value->desc;
			$temp[$i]->amount = $value->amount;
			$temp[$i]->type = $value->type;

			$i++;
		}
		$data['query'] = $temp;
		$this->load->view('components/htmlheader');
		$this->load->view('admin/transaction/print-transaction', $data);
		$this->load->view('components/htmlfooter');
		$this->load->view('admin/transaction/js-transaction');
	}

	public function add()
	{
		//query
		$data['query'] = $this->TransactionModel->insert_entry();
		//flashdata

		$type = $_POST['type'];
		redirect(base_url('transaction/' . $type), 'refresh');
	}

	public function income()
	{
		$data['title'] = "Finance App";
		$data['categories'] = $this->CategoryModel->get_last_ten_entries();

		$data['headerContent'] = "Pemasukan";
		$data['active'] = "Pemasukan";
		//query
		$dataRaw = $this->TransactionModel->get_entries('income');

		$i = 0;
		foreach ($dataRaw as $value) {
			$category = $this->CategoryModel->get_name($value->category_id);

			$temp[$i] = new StdClass;
			$temp[$i]->id = $value->id;

			$temp[$i]->category_id = $category[0]->name;
			$temp[$i]->desc = $value->desc;
			$temp[$i]->amount = $value->amount;
			$i++;
		}
		$data['query'] = $temp;
		//print_r($data['query']);
		$data['content'] = $this->load->view('admin/transaction/add-income', $data, TRUE);


		$this->load->view('main_layout', $data);
	}

	public function expense()
	{
		$data['title'] = "Finance App";
		$data['categories'] = $this->CategoryModel->get_last_ten_entries();

		$data['headerContent'] = "Pengeluaran";
		$data['active'] = "Pengeluaran";

		//query
		$dataRaw = $this->TransactionModel->get_entries('expense');

		$i = 0;
		foreach ($dataRaw as $value) {
			$category = $this->CategoryModel->get_name($value->category_id);

			$temp[$i] = new StdClass;
			$temp[$i]->id = $value->id;

			$temp[$i]->category_id = $category[0]->name;
			$temp[$i]->desc = $value->desc;
			$temp[$i]->amount = $value->amount;
			$i++;
		}
		$data['query'] = $temp;

		$data['content'] = $this->load->view('admin/transaction/add-expense', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function edit($id)
	{
		$data['title'] = "Finance App";
		$data['categories'] = $this->CategoryModel->get_last_ten_entries();

		$data['headerContent'] = "Transaksi | Edit";
		$data['active'] = "Transaksi";
		//query edit
		$data['query'] = $this->TransactionModel->get_id($id);

		$data['content'] = $this->load->view('admin/transaction/edit-transaction', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function delete($id)
	{
		//query
		$data['query'] = $this->TransactionModel->delete_entry($id);
		//flashdata

		$previous = "javascript:history.go(-1)";
		if (isset($_SERVER['HTTP_REFERER'])) {
			$previous = $_SERVER['HTTP_REFERER'];
		}
		redirect($previous, 'refresh');
	}

	public function update($id)
	{
		//query
		$data['query'] = $this->TransactionModel->update_entry($id);
		//flashdata

		redirect(base_url('transaction/') . $_POST['type'], 'refresh');
	}
}
