<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('TransactionModel');
		$this->load->model('CategoryModel');
		$this->load->model('UserModel');

		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}
	}

	public function index()
	{
		$data['title'] = "Finance App";
		$data['headerContent'] = "Home";
		$data['active'] = "Dashboard";

		$data['total_transaction'] = $this->TransactionModel->get_total_entries();
		$data['user'] = $this->UserModel->get_id($this->session->userdata('id'));


		$amount = $this->TransactionModel->get_total_amount();
		$income = $this->TransactionModel->get_total_income();
		$expense = $this->TransactionModel->get_total_expense();
		$data['amount'] = $amount[0]->amount;
		$data['income'] = $income[0]->amount;
		$data['expense'] = $expense[0]->amount;

		$data['content'] = $this->load->view('admin/dashboard', $data, TRUE);
		$this->load->view('main_layout', $data);
	}
}
