<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = "Pospay";
		$data['headerContent'] = "Login";

		$data['content'] = $this->load->view('main/flash', $data, TRUE);
		$this->load->view('public_layout', $data);
	}
}
