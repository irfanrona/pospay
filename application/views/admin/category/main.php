﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="card card-plain">
                        <div class="card-header card-header-icon" data-background-color="blue">
                            <i class="material-icons">view_list</i>
                        </div>
                        <h4 class="card-title">Tabel Kategori</h4>
                        <p class="category">List kategori</p>
                        <div class="card-content table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Aksi</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($query as $row) {
                                        $i++;
                                        $url = base_url('category/delete/') . $row->id;
                                    ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $row->name; ?></td>
                                            <td class="td-actions">
                                                <a href="<?= base_url('category/edit/') . $row->id; ?>">
                                                    <button type="button" rel="tooltip" class="btn btn-success">
                                                        <i class="material-icons">edit</i>
                                                    </button>
                                                </a>
                                                <button type="button" rel="tooltip" class="btn btn-danger">
                                                    <i class="material-icons" onclick="demo.showSwal('warning-message-and-cancel','<?= $url ?>')">close</i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class=" col-md-2">
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">add</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Tambah Kategori</h4>
                            <form method="post" action="<?php echo base_url() ?>category/add">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nama</label>
                                    <input class="form-control" type="text" name="name" required="true" />
                                </div>
                                <button type="submit" class="btn btn-fill btn-rose">Tambahkan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>