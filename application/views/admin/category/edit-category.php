﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">edit</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Edit Kategori</h4>
                            <form method="post" action="<?php echo base_url('category/update/') . $query[0]->id; ?>">
                                <input type="hidden" name="created_at" value="<?= $query[0]->created_at; ?>" />
                                <div class="form-group label-floating">
                                    <label class="control-label">Nama Kategori</label>
                                    <input class="form-control" type="text" name="name" required="true" value="<?= $query[0]->name; ?>" />
                                </div>
                                <button type="submit" class="btn btn-fill btn-rose">Sunting</button>
                                <a href="<?= $previous; ?>">
                                    <button type="submit" class="btn btn-secondary">Cancel</button>
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>