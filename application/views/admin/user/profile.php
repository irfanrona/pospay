﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header" data-background-color="rose">
                            POSPAY
                        </div>
                        <div class="card-content">
                            <h4 class="card-title"><?= $query[0]->fullname; ?></h4>
                            <br>
                            <p>GIROPOS LITE</p>
                            <p><?= $query[0]->phonenumber; ?></p>

                        </div>
                    </div>

                    <div class="bnt-simple"><b> Akun</b> </div>
                    <a href="<?php echo base_url('user/verifikasiAkun/') . $query[0]->id; ?>" class="btn btn-white col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">Verifikasi Akun</div>
                        <div class="col-md-2"><i class="material-icons">keyboard_arrow_right</i></div>
                    </a>
                    <a href="<?php echo base_url('user/profilSaya/') . $query[0]->id; ?>" class="btn btn-white col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">Profil Saya</div>
                        <div class="col-md-2"><i class="material-icons">keyboard_arrow_right</i></div>
                    </a>

                    <div class="bnt-simple"><b> Informasi Akun</b> </div>
                    <a href="#" class="btn btn-white col-md-12">
                        <div class="col-md-2">
                            <h4 class="material-icons text-danger">account_balance_wallet</h4>
                        </div>
                        <div class="col-md-8 text-danger">Saldo <br> <small class="text-muted">Rp<?= $query[0]->saldo; ?></small></div>
                        <div class="col-md-2"></div>
                    </a>
                    <a href="#" class="btn btn-white col-md-12">
                        <div class="col-md-2">
                            <h4 class="material-icons text-danger">monetization_on</h4>
                        </div>
                        <div class="col-md-8 text-danger">Pospay Coin <br> <small class="text-muted"><?= $query[0]->poin; ?></small></div>
                        <div class="col-md-2"></div>
                    </a>
                    <a href="#" class="btn btn-white col-md-12">
                        <div class="col-md-2">
                            <h4 class="material-icons text-danger">redeem</h4>
                        </div>
                        <div class="col-md-8 text-danger">Pospay Voucher <br> <small class="text-muted">Transaksi lebih mudah dengan Voucher</small></div>
                        <div class="col-md-2"></div>
                    </a>
                    <a href="#" class="btn btn-white col-md-12">
                        <div class="col-md-2">
                            <h4 class="material-icons text-danger">QR</h4>
                        </div>
                        <div class="col-md-8 text-danger">QR Codeku <br> <small class="text-muted">klik untuk menampilkan qr code</small></div>
                        <div class="col-md-2"></div>
                    </a>

                    <a href="<?= base_url('auth/logout') ?>" class="btn btn-rose btn-round col-md-12">Log out</a>

                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>