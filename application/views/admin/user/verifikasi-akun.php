﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card">

                        <div class="card-content">
                            <p>Verifikasi akun kamu agar dapat menggunakan fitur transfer, penarikan & weselpos instan</p>

                        </div>
                    </div>

                    <div class="card card-stats card-plain">
                        <div class="card-header" data-background-color="green">
                            <i class="material-icons">check</i>
                        </div>
                        <div class="card-content">
                            <h3 class=" card-title">Nomor Handphone</h3>
                            <p class="category"><?php echo $query[0]->phonenumber; ?></p>
                        </div>
                    </div>
                    <div class="card card-stats card-plain">
                        <div class="card-header" data-background-color="green">
                            <i class="material-icons">check</i>
                        </div>
                        <div class="card-content">
                            <h3 class=" card-title">Email</h3>
                            <p class="category"><?php echo $query[0]->email; ?></p>
                        </div>
                    </div>
                    <div class="card card-stats card-plain">
                        <div class="card-header" data-background-color="red">
                            <i class="material-icons">close</i>
                        </div>
                        <div class="card-content">
                            <h3 class=" card-title">Kartu Identitas</h3>
                        </div>
                    </div>
                    <a href="<?php echo base_url('user/profile/') . $query[0]->id; ?>" class="btn btn-rose btn-round col-md-12">Kembali</a>

                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>