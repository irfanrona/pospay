﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8">
                    <div class="card card-plain">
                        <div class="card-header card-header-icon" data-background-color="blue">
                            <i class="material-icons">person</i>
                        </div>
                        <h4 class="card-title">Tabel User</h4>
                        <p class="category">List User</p>
                        <div class="card-content table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>Nama Lengkap</th>
                                    <th>No. Telp</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Role</th>
                                    <th>Saldo</th>
                                    <th>Poin</th>
                                    <th>Aksi</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($query as $row) {
                                        $i++;
                                        $url = base_url('user/delete/') . $row->id;
                                    ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $row->fullname; ?></td>
                                            <td><?= $row->phonenumber; ?></td>
                                            <td><?= $row->username; ?></td>
                                            <td><?= $row->email; ?></td>
                                            <td><?php if ($row->is_active) {
                                                    echo "Aktif";
                                                } else {
                                                    echo "Tidak Aktif";
                                                } ?></td>
                                            <td><?php if ($row->role == 'admin') {
                                                    echo "Admin";
                                                } else {
                                                    echo "User";
                                                } ?></td>
                                            <td>Rp<?= $row->saldo; ?></td>
                                            <td><?= $row->poin; ?></td>
                                            <td class="td-actions">
                                                <a href="<?= base_url('user/edit/') . $row->id; ?>">
                                                    <button type="button" rel="tooltip" class="btn btn-success">
                                                        <i class="material-icons">edit</i>
                                                    </button>
                                                </a>
                                                <button type="button" rel="tooltip" class="btn btn-danger">
                                                    <i class="material-icons" onclick="demo.showSwal('warning-message-and-cancel','<?= $url ?>')">close</i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">add</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Tambah User</h4>
                            <form method="post" action="<?php echo base_url() ?>user/add">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nama</label>
                                    <input class="form-control" type="text" name="fullname" required="true" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">No. Telp</label>
                                    <input class="form-control" type="text" name="phonenumber" required="true" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Username</label>
                                    <input class="form-control" type="text" name="username" required="true" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Email</label>
                                    <input class="form-control" type="email" name="email" required="true" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Password</label>
                                    <input class="form-control" type="password" name="password" required="true" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Role</label>
                                    <select class="selectpicker" name="role" data-style="select-with-transition" title="User / Admin" data-size="3">
                                        <option disabled> Pilih Role</option>
                                        <option value="employee">User</option>
                                        <option value="admin">Admin</option>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Status</label>
                                    <select class="selectpicker" name="is_active" data-style="select-with-transition" title="Aktif / Tidak Aktif" data-size="3">
                                        <option disabled> Pilih Status</option>
                                        <option value="1">Aktif</option>
                                        <option value="0">Tidak Aktif</option>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Saldo</label>
                                    <input class="form-control" type="number" name="saldo" required="true" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Poin</label>
                                    <input class="form-control" type="number" name="poin" required="true" />
                                </div>
                                <button type="submit" class="btn btn-fill btn-rose">Tambahkan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>