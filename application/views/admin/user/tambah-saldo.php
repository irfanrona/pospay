﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">trending_up</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Informasi</h4>
                            <p>Anda dapat melakukan penambahan saldo Giropos dengan metode berikut ini</p>

                        </div>
                    </div>
                    <div class="card">
                        <div class="card-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <!-- POS INDONESIA -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                            <h4 class="panel-title">
                                                Pos Indonesia
                                                <i class="material-icons">keyboard_arrow_down</i>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <ol type="1">
                                                <li>Datang ke <b>Kantorpos</b> terdekat</li>
                                                <li>Ambil formulir penambahan saldo</li>
                                                <li>Isikan <b><?php echo $query[0]->phonenumber; ?></b> sebagai rekening tujuan</li>
                                                <li>Siapkan jumlah top up yang ingin dibayarkan</li>
                                                <li>Berikan formulir dan uang kepada petugas</li>

                                            </ol>
                                            <br>
                                            <small>Catatan: Minimun transaksi top up adalah Rp.10.000</small>
                                        </div>
                                    </div>
                                </div>
                                <!-- BANK BNI -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <h4 class="panel-title">
                                                Bank BNI
                                                <i class="material-icons">keyboard_arrow_down</i>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <b>ATM BNI</b>
                                            <ol type="1">
                                                <li>Masukkan kartu ATM dan PIN BNI Anda</li>
                                                <li>Pilih menu <b>TRANSFER ANTAR BNI</b> </li>
                                                <li>Masukan kode Giro <b><?php echo $query[0]->phonenumber; ?></b> dan rekening Giro Anda sebagai rekening tujuan (Contoh : <b><?php echo $query[0]->phonenumber; ?> 0103979043</b></li>
                                                <li>Masukan jumlah top up yang ingin dibayarkan</li>
                                                <li>Ikuti instruksi untuk menyelesaikan transaksi</li>

                                            </ol>
                                            <br>
                                            <small>Catatan:
                                                <li>Minimun transaksi top up adalah Rp.10.000</li>
                                                <li>Penambahan Saldo Giro dikenakan bea admin sebesar Rp2.000</li>
                                                <li>Top up tidak bisa dilakukan, jika nominal top up setelah dikurangi biaya admin kurang dari Rp10.000</li>
                                            </small>
                                            <br>
                                            <b>Internet Banking BNI</b>
                                            <ol type="1">
                                                <li>Masuk ke website <b>INTERNET BANKING BNI</b></li>
                                                <li>Pilih menu <b>TRANKSAKSI>TRANNSFER ANTAR BNI</b> </li>
                                                <li>Masukan kode Giro <b><?php echo $query[0]->phonenumber; ?></b> dan nomor rekening Anda sebagai nomor VA/nomor biling (Contoh : <b><?php echo $query[0]->phonenumber; ?> 0103979043</b></li>
                                                <li>Masukan jumlah top up yang ingin dibayarkan</li>
                                                <li>Ikuti instruksi untuk menyelesaikan transaksi</li>

                                            </ol>
                                            <br>
                                            <small>Catatan:
                                                <li>Minimun transaksi top up adalah Rp.10.000</li>
                                                <li>Penambahan Saldo Giro dikenakan bea admin sebesar Rp2.000</li>
                                                <li>Top up tidak bisa dilakukan, jika nominal top up setelah dikurangi biaya admin kurang dari Rp10.000</li>
                                            </small>
                                            <br>
                                            <b>SMS Banking (BNI MOBILE)</b>
                                            <ol type="1">
                                                <li>Masuk ke aplikasi mobile <b>BNI SMS BANKING</b></li>
                                                <li>Pilih menu <b>TRANKSAKSI>TRANNSFER ANTAR BNI</b> </li>
                                                <li>Masukan kode Giro <b><?php echo $query[0]->phonenumber; ?></b> dan Nomor Rekening Giro Anda sebagai rekening tujuan (Contoh : <b><?php echo $query[0]->phonenumber; ?> 0103979043</b></li>
                                                <li>Masukan jumlah top up yang ingin dibayarkan</li>
                                                <li>Ikuti instruksi untuk menyelesaikan transaksi</li>

                                            </ol>
                                            <br>
                                            <small>Catatan:
                                                <li>Minimun transaksi top up adalah Rp.10.000</li>
                                                <li>Penambahan Saldo Giro dikenakan bea admin sebesar Rp2.000</li>
                                                <li>Top up tidak bisa dilakukan, jika nominal top up setelah dikurangi biaya admin kurang dari Rp10.000</li>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                <!-- BANK BCA -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <h4 class="panel-title">
                                                Bank BCA
                                                <i class="material-icons">keyboard_arrow_down</i>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                        <div class="panel-body">
                                            <b>ATM BCA</b>
                                            <ol type="1">
                                                <li>Masukkan kartu ATM dan PIN BCA Anda</li>
                                                <li>Pilih menu <b>TRANSFER ANTAR BCA</b> </li>
                                                <li>Masukan kode Giro <b><?php echo $query[0]->phonenumber; ?></b> dan rekening Giro Anda sebagai rekening tujuan (Contoh : <b><?php echo $query[0]->phonenumber; ?> 0103979043</b></li>
                                                <li>Masukan jumlah top up yang ingin dibayarkan</li>
                                                <li>Ikuti instruksi untuk menyelesaikan transaksi</li>

                                            </ol>
                                            <br>
                                            <small>Catatan:
                                                <li>Minimun transaksi top up adalah Rp.10.000</li>
                                                <li>Penambahan Saldo Giro dikenakan bea admin sebesar Rp2.000</li>
                                                <li>Top up tidak bisa dilakukan, jika nominal top up setelah dikurangi biaya admin kurang dari Rp10.000</li>
                                            </small>
                                            <br>
                                            <b>KLIK BCA</b>
                                            <ol type="1">
                                                <li>Masuk ke website <b>KLIK BCAK</b></li>
                                                <li>Pilih menu <b>TRANKSAKSI DANA > TRANNSFER KE BCA VIRTUAL ACCOUNT</b> </li>
                                                <li>Masukan kode Giro <b><?php echo $query[0]->phonenumber; ?></b> dan nomor rekening Anda sebagai nomor VA/nomor biling (Contoh : <b><?php echo $query[0]->phonenumber; ?> 0103979043</b></li>
                                                <li>Masukan jumlah top up yang ingin dibayarkan</li>
                                                <li>Ikuti instruksi untuk menyelesaikan transaksi</li>

                                            </ol>
                                            <br>
                                            <small>Catatan:
                                                <li>Minimun transaksi top up adalah Rp.10.000</li>
                                                <li>Penambahan Saldo Giro dikenakan bea admin sebesar Rp2.000</li>
                                                <li>Top up tidak bisa dilakukan, jika nominal top up setelah dikurangi biaya admin kurang dari Rp10.000</li>
                                            </small>
                                            <br>
                                            <b>m-BCA (BNI MOBILE)</b>
                                            <ol type="1">
                                                <li>Masuk ke aplikasi mobile <b>m-BCA</b></li>
                                                <li>Pilih menu <b>M-TRANSFER > BCA VIRTUAL ACCOUNT</b> </li>
                                                <li>Masukan kode Giro <b><?php echo $query[0]->phonenumber; ?></b> dan Nomor Rekening Giro Anda sebagai rekening tujuan (Contoh : <b><?php echo $query[0]->phonenumber; ?> 0103979043</b></li>
                                                <li>Masukan jumlah top up yang ingin dibayarkan</li>
                                                <li>Masukan <b>PIN m-BCA</b></li>
                                                <li>Ikuti instruksi untuk menyelesaikan transaksi</li>

                                            </ol>
                                            <br>
                                            <small>Catatan:
                                                <li>Minimun transaksi top up adalah Rp.10.000</li>
                                                <li>Penambahan Saldo Giro dikenakan bea admin sebesar Rp2.000</li>
                                                <li>Top up tidak bisa dilakukan, jika nominal top up setelah dikurangi biaya admin kurang dari Rp10.000</li>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                <!-- BANK MANDIRI -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <h4 class="panel-title">
                                                Bank MANDIRI
                                                <i class="material-icons">keyboard_arrow_down</i>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false">
                                        <div class="panel-body">
                                            <b>ATM MANDIRI</b>
                                            <ol type="1">
                                                <li>Masukkan kartu ATM dan PIN BNI Anda</li>
                                                <li>Pilih menu <b>BAYAR/BELI</b> </li>
                                                <li>Pilih menu <b>MULTIPAYMENT</b> </li>
                                                <li>Pilih kode institusi <b>PT Pos Indonesia (Persero)</b> </li>
                                                <li>Ketik Kode Perusahaan yaitu <b>"88588" (POS INDONESIA)</b> ATAU</li>
                                                <li>Ketik <b>DAFTAR KODE</b> untuk mencari kode <b>POS INDONESIA</b> yaitu <b>88588</b></li>
                                                <li>Masukan kode Giro <b><?php echo $query[0]->phonenumber; ?></b> dan nomor rekening Giro Anda sebagai Virutal Account, kemudian tekan BENAR (Contoh : <b><?php echo $query[0]->phonenumber; ?> 0103979043</b></li>
                                                <li>Muncul konfirmasi data customer, Pilih Nomor 1 sesuai tagihan yang akan dibayar, kemudian tekan <b>YA</b></li>
                                                <li>Muncul konfirmasi pembayaran, tekan <b>YA</b> untuk melakukan pembayaran</li>
                                                <li>Bukti pembayaran dalam bentuk STRUK agar disimpan sebagai bukti pembayaran yang sah dari bank Mandiri</li>

                                            </ol>
                                            <br>
                                            <small>Catatan:
                                                <li>Minimun transaksi top up adalah Rp.10.000</li>
                                                <li>Penambahan Saldo Giro dikenakan bea admin sebesar Rp2.000</li>
                                                <li>Top up tidak bisa dilakukan, jika nominal top up setelah dikurangi biaya admin kurang dari Rp10.000</li>
                                            </small>
                                            <br>
                                            <b>MANDIRI MOBILE</b>
                                            <ol type="1">
                                                <li>Masuk <b>MANDIRI ONLINE</b></li>
                                                <li>Pilih menu <b>PEMBAYARAN</b> </li>
                                                <li>Pilih menu <b>MULTIPAYMENT</b> </li>
                                                <li>Pilih kode institusi <b>PT Pos Indonesia (Persero)</b> </li>
                                                <li>Masukan kode Giro <b><?php echo $query[0]->phonenumber; ?></b> dan nomor rekening Anda sebagai nomor VA/nomor biling (Contoh : <b><?php echo $query[0]->phonenumber; ?> 0103979043</b></li>
                                                <li>Masukan jumlah top up yang ingin dibayarkan</li>
                                                <li>Ikuti instruksi untuk menyelesaikan transaksi</li>

                                            </ol>
                                            <br>
                                            <small>Catatan:
                                                <li>Minimun transaksi top up adalah Rp.10.000</li>
                                                <li>Penambahan Saldo Giro dikenakan bea admin sebesar Rp2.000</li>
                                                <li>Top up tidak bisa dilakukan, jika nominal top up setelah dikurangi biaya admin kurang dari Rp10.000</li>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>