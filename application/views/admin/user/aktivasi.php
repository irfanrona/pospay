﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">face</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Informasi</h4>
                            <p>Saat ini akun POSPAY Anda masih pada akun Lite dan saldo maksimalmu Rp2.000.000. Lengkapi datamu untuk mengubah akunmu menjadi LITE PLUS dan menikmati layanan POSPAY Mobile lainnya</p>

                        </div>
                    </div>

                    <div class="card card-stats card-plain">
                        <div class="card-header" data-background-color="orange">
                            <i class="material-icons">account_balance</i>
                        </div>
                        <div class="card-content">
                            <h3 class=" card-title">Penyimpanan Saldo</h3>
                            <p class="category">Batas maksimum penyimpanan saldo Giropos menjadi Rp10.000.000</p>
                        </div>
                    </div>
                    <div class="card card-stats card-plain">
                        <div class="card-header" data-background-color="orange">
                            <i class="material-icons">account_balance_wallet</i>
                        </div>
                        <div class="card-content">
                            <h3 class=" card-title">Tarik Tunai</h3>
                            <p class="category">Lebih mudah tarik tunai di mana saja dan kapan saja.</p>
                        </div>
                    </div>
                    <div class="card card-stats card-plain">
                        <div class="card-header" data-background-color="orange">
                            <i class="material-icons">payment</i>
                        </div>
                        <div class="card-content">
                            <h3 class=" card-title">Transfer</h3>
                            <p class="category">Kamu bisa transfer ke mana saja. ke semua Bank.</p>
                        </div>
                    </div>
                    <a href="#" class="btn btn-rose btn-round col-md-12">Lanjutkan</a>

                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>