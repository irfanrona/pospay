﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card card-pricing card-plain">
                        <div class="content">
                            <h6 class="category">Fitur Belum Tersedia</h6>
                            <div class="icon">
                                <i class="material-icons">weekend</i>
                            </div>
                            <!-- <h3 class="card-title">FREE</h3> -->
                            <br>
                            <p class="card-description">
                                Segera upgrade rekening giromu menjadi LITEPLUS untuk dapat menggunakan layanan ini.
                            </p>
                            <a href="<?php echo base_url('admin'); ?>" class="btn btn-white btn-round">Nanti Saja</a>
                            <a href="<?php echo base_url('user/aktivasi/') . $query[0]->id; ?>" class="btn btn-rose btn-round">Aktivasi Sekarang</a>
                        </div>
                    </div>

                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>