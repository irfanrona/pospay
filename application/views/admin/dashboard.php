<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="red">
                        <i class="material-icons">local_atm</i>
                    </div>
                    <br>
                    <div class="timeline-heading">
                        <h4><?= $this->session->userdata('fullname'); ?>
                        </h4>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <p class="category"> GIROPOS LITE</p>
                                    <h3 class="card-title"><?php echo $user[0]->phonenumber; ?></h3>
                                </div>
                                <div class="col-md-6">
                                    <p class="category">Saldo</p>
                                    <h3 class="card-title">Rp<?php echo $user[0]->saldo; ?></h3>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            Poin:
                            <i class="material-icons text-danger">copyright</i>
                            <div class="timeline-heading">
                                <span class="label label-danger"><?php echo $user[0]->poin; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10 col-md-offset-2 col-xs-12">
                        <div class="col-md-3 col-xs-4">
                            <div class="card card-pricing card-raised">
                                <div class="content">
                                    <a href="<?php echo base_url('user/topup/') . $user[0]->id; ?>">
                                        <h3 class="card-title"><i class="material-icons">trending_up</i></h3>
                                        <p class="card-description">
                                            Top-up
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-4">
                            <div class="card card-pricing card-raised">
                                <div class="content">
                                    <a href="<?php echo base_url('user/penarikan/') . $user[0]->id; ?>">
                                        <h3 class="card-title"><i class="material-icons">input</i></h3>
                                        <p class="card-description">
                                            Penarikan
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-4">
                            <div class="card card-pricing card-raised">
                                <div class="content">
                                    <a href="<?php echo base_url('user/transfer/') . $user[0]->id; ?>">
                                        <h3 class="card-title"><i class="material-icons">sync</i></h3>
                                        <p class="card-description">
                                            Transfer
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- fitur utama -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">
                            <small> Fitur Utama</small>
                        </h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-3 col-sm-3">
                                        <button class="btn btn-just-icon btn-rose">
                                            <i class="material-icons">work</i>
                                        </button>
                                        <p class="">Wesel Instan</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-3 col-sm-3">
                                        <a href="<?php echo base_url('pln') ?>">
                                            <button class="btn btn-just-icon btn-warning">
                                                <i class="material-icons">flash_on</i>
                                            </button>
                                            <p class="">PLN </p><br>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-3 col-sm-3">
                                        <button class="btn btn-just-icon btn-info">
                                            <i class="material-icons">invert_colors</i>
                                        </button>
                                        <p class="">PDAM </p><br>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-3 col-sm-3">
                                        <button class="btn btn-just-icon btn-success">
                                            <i class="material-icons">spa</i>
                                        </button>
                                        <p class="">BPJS </p><br>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-3 col-sm-3">
                                        <button class="btn btn-just-icon btn-danger">
                                            <i class="material-icons">wifi</i>
                                        </button>
                                        <p class="">Pulsa Data</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-3 col-sm-3">
                                        <button class="btn btn-just-icon btn-info">
                                            <i class="material-icons">smartphone</i>
                                        </button>
                                        <p class="">Telco </p><br>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-3 col-sm-3">
                                        <button class="btn btn-just-icon btn-primary">
                                            <i class="material-icons">local_mall</i>
                                        </button>
                                        <p class="">Belanja Online</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-3 col-sm-3">
                                        <button class="btn btn-just-icon">
                                            <i class="material-icons">more_horiz</i>
                                        </button>
                                        <p class="">Lainya </p><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
        <!-- fitur lainnya -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">
                            <small> Layanan POS Lainnya</small>
                        </h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-1 col-sm-1">
                                        <a href="https://qposinaja.posindonesia.co.id/">
                                            <button class="btn btn-just-icon btn-danger">
                                                <i class="material-icons">send</i>
                                            </button>
                                            <p class="">PosAja</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-1 col-sm-1">
                                        <a href="https://tnt.posindonesia.co.id/">
                                            <button class="btn btn-just-icon btn-danger">
                                                <i class="material-icons">place</i>
                                            </button>
                                            <p class="">LACAK</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-1 col-sm-1">
                                        <a href="https://filateli.co.id/">
                                            <button class="btn btn-just-icon btn-danger">
                                                <i class="fa fa-facebook "></i>
                                            </button>
                                            <p class="">FILATELI</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="card-content">
                                    <div class="col-md-1 col-sm-1">
                                        <a href="https://meterai.posindonesia.co.id/">
                                            <button class="btn btn-just-icon btn-warning">
                                                <i class="fa fa-medium"></i>
                                            </button>
                                            <p class="">METERAI</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>