<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar-->
                    <button onclick="window.print();" type="button" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                        <i class="material-icons">print</i> Click for print
                        <div class="ripple-container"></div>
                    </button>
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="blue">
                        Laporan Transaksi
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kategori</th>
                                        <th>Deskripsi</th>
                                        <th>Jenis</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($query as $row) {
                                        $i++;
                                        $url = base_url('transaction/delete/') . $row->id;
                                    ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $row->category_id; ?></td>
                                            <td><?= $row->desc; ?></td>
                                            <td><?php if ($row->type == 'expense') {
                                                    echo "Pengeluaran";
                                                } else {
                                                    echo "Pemasukan";
                                                } ?></td>
                                            <td><?= $row->amount; ?></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>

    </div>
</div>