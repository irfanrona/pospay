﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">edit</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Edit Transaksi</h4>
                            <form method="post" action="<?php echo base_url('transaction/update/') . $query[0]->id; ?>">
                                <input type="hidden" name="created_at" value="<?= $query[0]->created_at; ?>" />
                                <div class="form-group label-floating">
                                    <label class="control-label">Jenis</label>
                                    <select class="selectpicker" name="type" data-style="select-with-transition" title="Pemasukan / Pengeluaran" data-size="7">
                                        <option disabled> Pilih Jenis</option>
                                        <option value="income" <?php if ($query[0]->type == 'income') {
                                                                    echo 'selected';
                                                                } ?>>Pemasukan</option>
                                        <option value="expense" <?php if ($query[0]->type == 'expense') {
                                                                    echo 'selected';
                                                                } ?>>Pengeluaran</option>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Kategori</label>
                                    <select class="selectpicker" name="category_id" data-style="select-with-transition" title="Kategori" data-size="7">
                                        <option disabled> Pilih Kategori</option>
                                        <?php
                                        foreach ($categories as $row) {
                                        ?>
                                            <option value="<?= $row->id ?>" <?php if ($query[0]->category_id == $row->id) {
                                                                                echo 'selected';
                                                                            } ?>><?= $row->name ?> </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Deskripsi</label>
                                    <input class="form-control" type="text" name="desc" required="true" value="<?= $query[0]->desc; ?>" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Jumlah</label>
                                    <input class="form-control" type="number" name="amount" required="true" value="<?= $query[0]->amount; ?>" />
                                </div>
                                <button type="submit" class="btn btn-fill btn-rose">Sunting</button>
                                <a href="<?= $previous; ?>">
                                    <button type="submit" class="btn btn-secondary">Cancel</button>
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>