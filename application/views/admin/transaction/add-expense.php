﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8">
                    <div class="card card-plain">
                        <div class="card-header card-header-icon" data-background-color="blue">
                            <i class="material-icons">assignment</i>
                        </div>
                        <h4 class="card-title">Tabel Pengeluaran</h4>
                        <p class="category">List seluruh pengeluaran</p>
                        <div class="card-content table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>Kategori</th>
                                    <th>Deskripsi</th>
                                    <th>Jumlah</th>
                                    <th>Aksi</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($query as $row) {
                                        $i++;
                                        $url = base_url('transaction/delete/') . $row->id;
                                    ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $row->category_id; ?></td>
                                            <td><?= $row->desc; ?></td>
                                            <td><?= $row->amount; ?></td>
                                            <td class="td-actions">
                                                <a href="<?= base_url('transaction/edit/') . $row->id; ?>">
                                                    <button type="button" rel="tooltip" class="btn btn-success">
                                                        <i class="material-icons">edit</i>
                                                    </button>
                                                </a>
                                                <button type="button" rel="tooltip" class="btn btn-danger">
                                                    <i class="material-icons" onclick="demo.showSwal('warning-message-and-cancel','<?= $url ?>')">close</i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">add</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Tambah Pengeluaran</h4>
                            <form method="post" action="<?php echo base_url() ?>transaction/add">
                                <input type="hidden" name="type" value="expense" />
                                <div class="form-group label-floating">
                                    <select class="selectpicker" name="category_id" data-style="select-with-transition" title="Kategori" data-size="7">
                                        <option disabled> Pilih Kategori</option>
                                        <?php
                                        foreach ($categories as $row) {
                                        ?>
                                            <option value="<?= $row->id ?>"><?= $row->name ?> </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Deskripsi</label>
                                    <input class="form-control" type="text" name="desc" required="true" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Jumlah</label>
                                    <input class="form-control" type="number" name="amount" required="true" />
                                </div>
                                <button type="submit" class="btn btn-fill btn-rose">Tambahkan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>