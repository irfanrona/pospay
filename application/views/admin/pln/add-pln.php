﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">flash_on</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">PLN</h4>
                            <form method="post" action="<?php echo base_url('pln/add'); ?>">
                                <input class="form-control" type="hidden" name="id_user" value="<?= $this->session->userdata('id') ?>" />
                                <div class="form-group label-floating">
                                    <label class="control-label">Jenis Produk</label>
                                    <select class="selectpicker" name="jenis" data-style="select-with-transition" title="Pilih Jenis Produk" data-size="5">
                                        <option disabled> Pilih Jenis Produk</option>
                                        <option value="TAGIHAN PLN">TAGIHAN PLN</option>
                                        <option value="TOKEN PLN">TOKEN PLN</option>
                                        <option value="PLN NONTAGLIS">PLN NONTAGLIS</option>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">No. Meteran / ID Pelanggan</label>
                                    <input class="form-control" type="text" name="id_pelanggan" required="true" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Jumlah</label>
                                    <input class="form-control" type="number" name="amount" required="true" />
                                </div>
                                <button type="submit" class="btn btn-fill btn-rose">Lanjutkan</button>
                                <a href="<?= $previous; ?>">
                                    <button type="submit" class="btn btn-secondary">Cancel</button>
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>