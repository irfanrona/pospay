﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <div class="card card-pricing card-plain">
                        <div class="content">
                            <h6 class="category">Status Transaksi</h6>
                            <div class="icon">
                                <?php if ($flashdata == "Transaksi Berhasil!") {
                                ?> <i class="material-icons">check</i> <?php
                                                                    } else {
                                                                        ?> <i class="material-icons">warning</i> <?php
                                                                                                                }
                                                                                                                    ?>
                            </div>
                            <!-- <h3 class="card-title">FREE</h3> -->
                            <br>
                            <p class="card-description">
                                <?= $flashdata; ?>
                            </p>
                            <a href="<?php echo base_url('admin'); ?>" class="btn btn-white btn-round">Kembali ke menu utama</a>
                            <?php if ($flashdata != "Transaksi Berhasil!") {
                            ?><a href="<?php echo base_url('user/topup/') . $this->session->userdata('id'); ?>" class="btn btn-rose btn-round">Isi Saldo Sekarang</a> <?php
                                                                                                                                                                    }
                                                                                                                                                                        ?>

                        </div>
                    </div>

                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>