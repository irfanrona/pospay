<body>
    <div class="wrapper">
        <div class="sidebar" data-active-color="blue" data-background-color="black" data-image="<?= base_url() ?>assets/img/sidebar-1.jpg">
            <div class="logo">
                <img class="logo-image" src="<?= base_url('assets/img/logo.png') ?>" alt="">
            </div>
            <div class="logo logo-mini">
                <center>
                    <img src="<?= base_url('assets/img/favicon.png') ?>" alt="">
                </center>
            </div>
            <div class="sidebar-wrapper">
                <div class="user">
                    <div class="photo">
                        <img src="<?= base_url() ?>assets/img/default-avatar.png" />
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                            <?= $this->session->userdata('fullname'); ?>
                        </a>
                    </div>
                </div>
                <ul class="nav">

                    <li <?php if ($active == 'Dashboard') { ?> class="active" <?php } ?>>
                        <a href="<?= base_url() ?>admin">
                            <i class="material-icons">home</i>
                            <p>Home</p>
                        </a>
                    </li>
                    <li <?php if ($active == 'Kategori') { ?> class="active" <?php } ?>>
                        <a href="#">
                            <i class="material-icons">star</i>
                            <p>Favorite</p>
                        </a>
                    </li>
                    <li <?php if ($active == 'Kategori') { ?> class="active" <?php } ?>>
                        <a href="#">
                            <i class="material-icons">view_list</i>
                            <p>Riwayat</p>
                        </a>
                    </li>
                    <li <?php if ($active == 'User') { ?> class="active" <?php } ?>>
                        <a href="<?= base_url('user/profile/') . $this->session->userdata('id'); ?>">
                            <i class="material-icons">person</i>
                            <p>Profile</p>
                        </a>
                    </li>

                    <?php if ($this->session->userdata('role') == 'admin') { ?>
                        <li <?php if ($active == 'UserManagement') { ?> class="active" <?php } ?>>
                            <a href="<?= base_url() ?>user">
                                <i class="material-icons">person</i>
                                <p>User Management</p>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> <?php echo $headerContent; ?> </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('user/profile/') . $this->session->userdata('id'); ?>" class="dropdown-toggle">
                                    <p class="hidden-sm hidden-sx"><?= $this->session->userdata('fullname'); ?></p>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>auth/logout">
                                    <i class="material-icons"><span class="material-icons-outlined">
                                            open_in_new
                                        </span></i>
                                    <p class="hidden-lg hidden-md">Logout</p>
                                </a>
                            </li>
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>
                    </div>
                </div>
            </nav>