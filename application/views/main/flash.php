<div class="wrapper wrapper-full-page">
    <div class="full-page login-page" filter-color="black" data-image="<?= base_url() ?>assets/img/login.jpg">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="tab-content">
                                <div class="tab-pane active" id="image-1">
                                    <div class="col-md-8 col-md-offset-2">

                                        <div class="card card-product">
                                            <div class="card-image" data-header-animation="true">
                                                <a href="#pablo">
                                                    <img class="img" src="<?= base_url('assets/img/card-2.jpg') ?>">
                                                </a>
                                            </div>
                                            <div class="card-content">
                                                <h4 class="card-title">
                                                    Transfer Uang ke Bank Mana Aja.
                                                </h4>
                                                <div class="card-description">
                                                    Sekarang kamu bisa transfer uang ke bank mana aja tanpa perlu keluar rumah!
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="image-2">
                                    <div class="col-md-8 col-md-offset-2">

                                        <div class="card card-product">
                                            <div class="card-image" data-header-animation="true">
                                                <a href="#pablo">
                                                    <img class="img" src="<?= base_url('assets/img/card-1.jpg') ?>">
                                                </a>
                                            </div>
                                            <div class="card-content">
                                                <h4 class="card-title">
                                                    Solusi Pembelian dan Pembayaran.
                                                </h4>
                                                <div class="card-description">
                                                    Sekarang kamu bisa melakukan pembelian dan pembayaran dengan mudah, hanya dengan menggunakan smartphonemu!
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="image-3">

                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="card card-product">
                                            <div class="card-image" data-header-animation="true">
                                                <a href="#pablo">
                                                    <img class="img" src="<?= base_url('assets/img/card-3.jpg') ?>">
                                                </a>
                                            </div>
                                            <div class="card-content">
                                                <h4 class="card-title">
                                                    Solusi Belanja Online Kekinian.
                                                </h4>
                                                <div class="card-description">
                                                    Sekarang kamu bisa melakukan pembayaran belanja online dengan mudah dan ringkas.
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="nav-center">
                                <ul class="nav nav-pills nav-pills-warning nav-pills-icons" role="tablist">
                                    <!--color-classes: "nav-pills-primary", "nav-pills-info", "nav-pills-success", "nav-pills-warning","nav-pills-danger"-->
                                    <li class="active">
                                        <a href="#image-1" role="tab" data-toggle="tab">
                                            <p>
                                                <i class="material-icons">
                                                    fiber_manual_record
                                                </i>
                                            </p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#image-2" role="tab" data-toggle="tab">
                                            <p>
                                                <i class="material-icons">
                                                    fiber_manual_record
                                                </i>
                                            </p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#image-3" role="tab" data-toggle="tab">
                                            <p>
                                                <i class="material-icons">
                                                    fiber_manual_record
                                                </i>
                                            </p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="text-center col-md-4 col-md-offset-4">
                            <a href="<?= base_url('auth');?>"> <button type="submit" class="btn btn-fill btn-rose">Lanjutkan</button> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </body>