<div class="wrapper wrapper-full-page">
    <div class="full-page register-page" filter-color="black" data-image="<?= base_url() ?>assets/img/register.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="card card-signup">
                        <h2 class="card-title text-center">Buat Akun</h2>
                        <div class="info info-horizontal">
                            <h4 class="info-title">Informasi</h4>
                            <p class="description">Saat ini Anda belum memiliki akun POSPAY. Silakan lengkapi data berikut untuk melanjutkan proses registrasi</p>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10">
                                <div class="social text-center">
                                    <h4 class="font-weight-bold text-warning"> <?= $this->session->flashdata('flash'); ?> </h4>
                                </div>
                                <form class="form" method="post" action="<?php echo base_url() ?>user/add">
                                    <div class="card-content">
                                        <input type="hidden" name="role" value="employee">
                                        <input type="hidden" name="is_active" value="0">
                                        <input type="hidden" name="saldo" value="0">
                                        <input type="hidden" name="poin" value="0">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">face</i>
                                            </span>
                                            <input type="text" name="fullname" class="form-control" placeholder="Nama Lengkap..." required="true">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">phone</i>
                                            </span>
                                            <input type="text" name="phonenumber" class="form-control" placeholder="No. Handphone..." required="true">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">fingerprint</i>
                                            </span>
                                            <input type="text" name="username" class="form-control" placeholder="Username..." required="true">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                                            <input type="email" name="email" class="form-control" placeholder="Email..." required="true">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                            <input type="password" name="password" placeholder="Password..." class="form-control" required="true" />
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                            <input type="password" name="repassword" placeholder="Konfirmasi Password..." class="form-control" required="true" />
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">code</i>
                                            </span>
                                            <input type="text" name="referal" class="form-control" placeholder="Kode Referal (optional)" required="false">
                                        </div>
                                    </div>
                                    <div class="footer text-center">
                                        <button type="submit" class="btn btn-primary btn-round">Registrasi sekarang</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </body>