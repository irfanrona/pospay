<div class="wrapper wrapper-full-page">
    <div class="full-page register-page" filter-color="black" data-image="<?= base_url() ?>assets/img/register.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="card card-signup">
                        <h2 class="card-title text-center">Lupa Password</h2>
                        <div class="info info-horizontal">
                            <h4 class="info-title">Informasi</h4>
                            <p class="description">Jika lupa password, Anda bisa melakukan beberapa proses berikut</p>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10">
                                <div class="social text-center">
                                    <h4 class="font-weight-bold text-warning"> <?= $this->session->flashdata('flash'); ?> </h4>
                                </div>
                                <form class="form" method="post" action="<?php echo base_url('auth/password/new')?>">
                                    <div class="card-content">

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">phone</i>
                                            </span>
                                            <input type="text" name="phonenumber" class="form-control" placeholder="No. Handphone..." required="true">
                                        </div>

                                    </div>
                                    <div class="footer text-center">
                                    <button type="submit" class="btn btn-primary btn-round">Lanjutkan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </body>