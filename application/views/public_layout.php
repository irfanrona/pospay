<?php
defined('BASEPATH') or exit('No direct script access allowed');


$this->load->view('components/htmlheader_pub');
$this->load->view('components/header_pub');

//content
echo $content;

$this->load->view('components/footer_pub');
$this->load->view('components/htmlfooter_pub');
