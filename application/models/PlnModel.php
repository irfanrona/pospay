<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PlnModel extends CI_Model
{

	public $id_user;
	public $jenis;
	public $id_pelanggan;
	public $amount;
	public $created_at;
	public $updated_at;

	public function get_last_ten_entries()
	{
		$query = $this->db->get('pln', 100);
		return $query->result();
	}

	public function get_total_entries()
	{
		$query = $this->db->get('pln');
		return $query->num_rows();
	}

	public function get_total_amount()
	{
		$this->db->select_sum('amount');
		$this->db->from('pln');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_entries()
	{
		$query = $this->db->get_where('pln');
		return $query->result();
	}

	public function get_id($id)
	{
		$query = $this->db->get_where('pln', array('id' => $id));
		return $query->result();
	}

	public function insert_entry()
	{
		$this->id_user = $_POST['id_user'];
		$this->jenis = $_POST['jenis'];
		$this->id_pelanggan = $_POST['id_pelanggan'];
		$this->amount = $_POST['amount'];
		$this->created_at = date('Y-m-d H:i:s');
		$this->updated_at = date('Y-m-d H:i:s');

		$this->db->insert('pln', $this);
	}

	public function update_entry($id)
	{
		$this->id_user = $_POST['id_user'];
		$this->jenis = $_POST['jenis'];
		$this->id_pelanggan = $_POST['id_pelanggan'];
		$this->amount = $_POST['amount'];
		$this->created_at = $_POST['created_at'];
		$this->updated_at = date('Y-m-d H:i:s');

		$this->db->update('pln', $this, array('id' => $id));
	}

	public function delete_entry($id)
	{
		$this->db->delete('pln', array('id' => $id));
	}
}
