<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserModel extends CI_Model
{
	public $phonenumber;
	public $username;
	public $email;
	public $password;
	public $remember_token;
	public $fullname;
	public $is_active;
	public $role;
	public $referal;
	public $saldo;
	public $poin;
	public $created_at;
	public $updated_at;

	public function get_last_ten_entries()
	{
		$query = $this->db->get('users', 100);
		return $query->result();
	}

	public function get_id($id)
	{
		$query = $this->db->get_where('users', array('id' => $id));
		return $query->result();
	}

	public function insert_entry()
	{
		$this->phonenumber    = $_POST['phonenumber'];
		$this->username    = $_POST['username'];
		$this->email    = $_POST['email'];
		$this->password    = $_POST['password'];
		$this->fullname    = $_POST['fullname'];
		$this->role    = $_POST['role'];
		$this->referal    = $_POST['referal'];
		$this->saldo    = $_POST['saldo'];
		$this->poin    = $_POST['poin'];
		$this->is_active    = $_POST['is_active'];
		$this->created_at  = date('Y-m-d H:i:s');
		$this->updated_at     = date('Y-m-d H:i:s');

		$this->db->insert('users', $this);
	}

	public function update_entry($id)
	{
		$this->phonenumber    = $_POST['phonenumber'];
		$this->username    = $_POST['username'];
		$this->email    = $_POST['email'];
		$this->password    = $_POST['password'];
		$this->fullname    = $_POST['fullname'];
		$this->role    = $_POST['role'];
		$this->referal    = $_POST['referal'];
		$this->saldo    = $_POST['saldo'];
		$this->poin    = $_POST['poin'];
		$this->is_active    = $_POST['is_active'];
		$this->created_at = $_POST['created_at'];
		$this->updated_at = date('Y-m-d H:i:s');

		$this->db->update('users', $this, array('id' => $id));
	}

	public function update_pass($id)
	{
		$this->password    = $_POST['password'];

		$this->db->update('users', array('password' => $this->password), array('id' => $id));
	}

	public function delete_entry($id)
	{
		$this->db->delete('users', array('id' => $id));
	}

	public function get_name($id)
	{
		$query = $this->db->get_where('users', array('id' => $id), 1);
		return $query->result();
	}

	public function get_username($username)
	{
		$query = $this->db->get_where('users', array('username' => $username), 1);
		return $query->result();
	}

	public function get_phonenumber($phonenumber)
	{
		$query = $this->db->get_where('users', array('phonenumber' => $phonenumber), 1);
		return $query->result();
	}

	public function set_saldo($id, $saldo)
	{

		$this->saldo    = $saldo;

		$this->db->update('users', array('saldo' => $this->saldo), array('id' => $id));
	}
}
