<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TransactionModel extends CI_Model
{

	public $category_id;
	public $desc;
	public $type;
	public $amount;
	public $created_at;
	public $updated_at;

	public function get_last_ten_entries()
	{
		$query = $this->db->get('transactions', 100);
		return $query->result();
	}

	public function get_total_entries()
	{
		$query = $this->db->get('transactions');
		return $query->num_rows();
	}

	public function get_total_amount()
	{
		$this->db->select_sum('amount');
		$this->db->from('transactions');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_total_income()
	{
		$this->db->select_sum('amount');
		$this->db->from('transactions');
		$this->db->where('type', 'income');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_total_expense()
	{
		$this->db->select_sum('amount');
		$this->db->from('transactions');
		$this->db->where('type', 'expense');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_entries($type)
	{
		$query = $this->db->get_where('transactions', array('type' => $type));
		return $query->result();
	}

	public function get_id($id)
	{
		$query = $this->db->get_where('transactions', array('id' => $id));
		return $query->result();
	}

	public function insert_entry()
	{
		$this->category_id = $_POST['category_id'];
		$this->desc = $_POST['desc'];
		$this->type = $_POST['type'];
		$this->amount = $_POST['amount'];
		$this->created_at = date('Y-m-d H:i:s');
		$this->updated_at = date('Y-m-d H:i:s');

		$this->db->insert('transactions', $this);
	}

	public function update_entry($id)
	{
		$this->category_id = $_POST['category_id'];
		$this->desc = $_POST['desc'];
		$this->type = $_POST['type'];
		$this->amount = $_POST['amount'];
		$this->created_at = $_POST['created_at'];
		$this->updated_at = date('Y-m-d H:i:s');

		$this->db->update('transactions', $this, array('id' => $id));
	}

	public function delete_entry($id)
	{
		$this->db->delete('transactions', array('id' => $id));
	}
}
